using FaroTricolore.Api.Services.Services;
using FaroTricolore.Api.Shared.Abstracts;
using FaroTricolore.Api.Shared.Abstracts.Services;
using Microsoft.Extensions.DependencyInjection;
using Simple.Shared.Abstracts;

namespace FaroTricolore.Api.Mediator
{
    public static class ServicesHelper
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            // Transient = created each time they're requested from the service container
            // Scoped = created once per client request
            // Singleton
            services.AddTransient<IEventService, EventService>();
            services.AddTransient<IMediaService, MediaService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IMigrationService, MigrationService>();

            services.AddScoped<IStorageService, GoogleStorageService>();
            services.AddScoped<IImageService, ImageService>();
            
            return services;
        }
    }
}