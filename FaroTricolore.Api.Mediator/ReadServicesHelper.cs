using FaroTricolore.Api.Services.ReadServices;
using FaroTricolore.Api.Shared.Abstracts.ReadServices;
using Microsoft.Extensions.DependencyInjection;

namespace FaroTricolore.Api.Mediator
{
    public static class ReadServicesHelper
    {
        public static IServiceCollection AddReadServices(this IServiceCollection services)
        {
            // Transient = created each time they're requested from the service container
            // Scoped = created once per client request
            // Singleton
            services.AddTransient<IEventReadService, EventReadService>();
            services.AddTransient<IMediaReadService, MediaReadService>();
            
            return services;
        }
    }
}