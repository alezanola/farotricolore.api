using FaroTricolore.Api.Services.Services;
using FaroTricolore.Api.Shared.Abstracts;
using FaroTricolore.Api.Shared.Abstracts.Services;
using Microsoft.Extensions.DependencyInjection;
using Simple.Shared.Abstracts;

namespace FaroTricolore.Api.Mediator
{
    public static class OrchestratorsHelper
    {
        public static IServiceCollection AddOrchestrators(this IServiceCollection services)
        {
            // Transient = created each time they're requested from the service container
            // Scoped = created once per client request
            // Singleton
            services.AddTransient<IEventService, EventService>();
            services.AddTransient<IMediaService, MediaService>();
            services.AddTransient<IUserService, UserService>();

            services.AddScoped<IStorageService, GoogleStorageService>();
            services.AddScoped<IImageService, ImageService>();
            
            return services;
        }
    }
}