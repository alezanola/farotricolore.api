using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.MongoDb.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace FaroTricolore.Api.Mediator
{
    public static class DataModelHelper
    {
        public static IServiceCollection AddDataModel(this IServiceCollection services)
        {

            services.AddTransient<IDocumentUnitOfWork, DocumentUnitOfWork>();

            return services;
        }
    }
}