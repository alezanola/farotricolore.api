namespace FaroTricolore.Api.Shared.Configuration
{
    public class ConnectionStrings
    {
        public string MongoDb { get; set; }
        public string DatabaseName { get; set; }
    }
}