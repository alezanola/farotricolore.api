namespace FaroTricolore.Api.Shared.Configuration
{
    public class TokenAuthentication
    {
        public string SecretKeyVariableName { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public int TokenExpirationHours { get; set; }
    }
}