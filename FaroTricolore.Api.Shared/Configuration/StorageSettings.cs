namespace FaroTricolore.Api.Shared.Configuration
{
    public class StorageSettings
    {
        public string CredentialJsonVariableName { get; set; }
        public string BasePathImages { get; set; }
        public string BasePathAppImages { get; set; }
        public string BucketName { get; set; }
        public string StorageBasePath { get; set; }
        
        public Simple.Shared.Configurations.StorageSettings ToShared()
        {
            return new Simple.Shared.Configurations.StorageSettings
            {
                CredentialJsonVariableName = CredentialJsonVariableName,
                BasePathImages = BasePathImages,
                BasePathAppImages = BasePathAppImages,
                BucketName = BucketName,
                StorageBasePath = StorageBasePath
            };
        }
    }
}