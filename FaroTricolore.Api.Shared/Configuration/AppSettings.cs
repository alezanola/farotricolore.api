﻿namespace FaroTricolore.Api.Shared.Configuration
{
    public class AppSettings
    {
	    public string BaseUrlApi { get; set; }
	    public string BaseUrlWeb { get; set; }
	    public CompressionQuality CompressionQuality { get; set; }
        public ConnectionStrings ConnectionStrings { get; set; }
        public StorageSettings Storage { get; set; }
        public TokenAuthentication TokenAuthentication { get; set; }
    }
}