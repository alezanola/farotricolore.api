namespace FaroTricolore.Api.Shared.Configuration
{
    public class CompressionQuality
    {
        public int LargeSize { get; set; }
        public int MediumSize { get; set; }
        public int SmallSize { get; set; }

        public Simple.Shared.Configurations.CompressionQuality ToShared()
        {
            return new Simple.Shared.Configurations.CompressionQuality
            {
                LargeSize = LargeSize,
                MediumSize = MediumSize,
                SmallSize = SmallSize
            };
        }
    }
}