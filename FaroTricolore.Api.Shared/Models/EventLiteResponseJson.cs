﻿using System;
namespace FaroTricolore.Api.Shared.Models
{
    public class EventLiteResponseJson
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Location { get; set; }
        public ImageJson Poster { get; set; }
        public bool HideEvent { get; set; }
    }
}
