﻿using System;
namespace FaroTricolore.Api.Shared.Models
{
    public class EventToAddJson
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PosterBase64 { get; set; }
        public string Location { get; set; }
        public bool? HideEvent { get; set; }
        public bool? RemovePoster { get; set; }
    }
}
