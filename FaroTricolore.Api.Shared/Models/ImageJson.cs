namespace FaroTricolore.Api.Shared.Models
{
    public class ImageJson
    {
        public string Id { get; set; }
        public string Small { get; set; }
        public string Medium { get; set; }
        public string Large { get; set; }
    }
}