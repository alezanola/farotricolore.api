namespace FaroTricolore.Api.Shared.Models
{
    public class UserCredentialsJson
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}