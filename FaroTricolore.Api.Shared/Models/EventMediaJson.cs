using System.Collections.Generic;

namespace FaroTricolore.Api.Shared.Models
{
    public class EventMediaJson
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<ImageJson> Images { get; set; }
        public IEnumerable<VideoJson> Videos { get; set; }
    }
}