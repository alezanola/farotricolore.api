using System;
using System.Collections.Generic;

namespace FaroTricolore.Api.Shared.Models
{
    public class EventDetailedJson
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Location { get; set; }
        public ImageJson Poster { get; set; }
        public bool HideEvent { get; set; }
        public bool MediaExist { get; set; }
    }
}