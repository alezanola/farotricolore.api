namespace FaroTricolore.Api.Shared.Models
{
    public class VideoJson
    {
        public string Id { get; set; }
        public string YouTubeCode { get; set; }
    }
}