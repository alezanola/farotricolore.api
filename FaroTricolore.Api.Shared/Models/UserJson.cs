namespace FaroTricolore.Api.Shared.Models
{
    public class UserJson
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}