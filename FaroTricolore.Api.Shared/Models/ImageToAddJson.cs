namespace FaroTricolore.Api.Shared.Models
{
    public class ImageToAddJson
    {
        public string Base64 { get; set; }
        public string Extension { get; set; }
    }
}