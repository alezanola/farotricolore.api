namespace FaroTricolore.Api.Shared.Models
{
    public class ErrorJson
    {
        public string Error { get; set; }
    }
}