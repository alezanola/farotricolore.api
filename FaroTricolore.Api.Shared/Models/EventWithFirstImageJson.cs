using System;

namespace FaroTricolore.Api.Shared.Models
{
    public class EventWithFirstImageJson
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public DateTime? StartDate { get; set; }
        public ImageJson FirstImage { get; set; }
    }
}