using System;

namespace FaroTricolore.Api.Shared.Models
{
    public class UserInfoJson
    {
        // email, Name, surname, token, tokenExpiration
        public string Email { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public DateTime TokenExpiration { get; set; }
    }
}