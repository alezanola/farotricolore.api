namespace FaroTricolore.Api.Shared.Models
{
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string EventManager = "EventManager";

        public static bool Exists(string role)
        {
            switch (role)
            {
                case EventManager:
                case Admin:
                    return true;
                
                default:
                    return false;
            }
        }
    }
}