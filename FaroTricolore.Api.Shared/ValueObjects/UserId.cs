using System;

namespace FaroTricolore.Api.Shared.ValueObjects
{
    public class UserId : DomainIdBase<UserId>
    {
        public UserId(Guid value) : base(value)
        {
        }
    }
}