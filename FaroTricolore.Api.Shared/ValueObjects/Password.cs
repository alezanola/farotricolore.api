using System;
using System.Security.Cryptography;
using System.Text;
using FaroTricolore.Api.Shared.ValueObjects.Base;

namespace FaroTricolore.Api.Shared.ValueObjects
{
    public class Password : ValueObjectBase<Password>
    {
        private string HashedValue { get; }
        
        public Password(string password)
        {
            try
            {
                using (var mySHA256 = SHA256.Create())
                {
                    // Check if is hashed
                    var hash = Convert.FromBase64String(password);
                    var hashValue = mySHA256.ComputeHash(hash);
                }

                HashedValue = password;
            }
            catch (Exception e)
            {
                HashedValue = HashPassword(password);
            }
        }
        
        private string HashPassword(string clearPassword)
        {
            var data = Encoding.UTF8.GetBytes(clearPassword);
            using (HashAlgorithm sha = new SHA256Managed())
            {
                sha.ComputeHash(data);
                sha.TransformFinalBlock(data, 0, data.Length);
                return Convert.ToBase64String(sha.Hash);
            }
        }

        public override void ChkIsValid()
        {
        }

        public string GetValue()
        {
            return HashedValue;
        }
    }
}