﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FaroTricolore.Api.Shared.ValueObjects.Base
{
    public abstract class ValueObjectBase<T> where T : ValueObjectBase<T>
    {
        public override bool Equals(object other)
        {
            return this.Equals(other as T);
        }

        protected virtual IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>();
        }

        public bool Equals(T other)
        {
            if (other == null) return false;

            return
                this.GetAttributesToIncludeInEqualityCheck()
                    .SequenceEqual(other.GetAttributesToIncludeInEqualityCheck());
        }

        public static bool operator ==(ValueObjectBase<T> left, ValueObjectBase<T> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ValueObjectBase<T> left, ValueObjectBase<T> right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            return this.GetAttributesToIncludeInEqualityCheck()
                .Aggregate(17, (current, obj) => current * 31 + (obj?.GetHashCode() ?? 0));
        }

        public abstract void ChkIsValid();

        protected void ThrowNotValidValue(string message)
        {
            var completeMessage = $"Error with {GetType().FullName} value: {message}";
            throw new Exception(completeMessage);
        }
    }
}