﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FaroTricolore.Api.Shared.ValueObjects.Base
{
    public abstract class ValueObjectBoolean<T> where T : ValueObjectBoolean<T>
    {
        private bool Value { get; }

        protected ValueObjectBoolean(bool value)
        {
            this.Value = value;
        }

        public override bool Equals(object other)
        {
            return this.Equals(other as T);
        }

        protected virtual IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>();
        }

        public bool Equals(T other)
        {
            if (other == null) return false;

            return
                this.GetAttributesToIncludeInEqualityCheck()
                    .SequenceEqual(other.GetAttributesToIncludeInEqualityCheck());
        }

        public static bool operator ==(ValueObjectBoolean<T> left, ValueObjectBoolean<T> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ValueObjectBoolean<T> left, ValueObjectBoolean<T> right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            return this.GetAttributesToIncludeInEqualityCheck()
                .Aggregate(17, (current, obj) => current * 31 + (obj?.GetHashCode() ?? 0));
        }

        public virtual bool GetValue()
        {
            return this.Value;
        }

        public virtual bool IsValid()
        {
            return Value != null;
        }

        public virtual void ChkIsValid(string message = "")
        {
            if (this.IsValid())
                return;

            if (string.IsNullOrEmpty(message))
                message = $"{GetType().FullName} is Required!";
            throw new ArgumentNullException(GetType().FullName, message);
        }
    }
}
