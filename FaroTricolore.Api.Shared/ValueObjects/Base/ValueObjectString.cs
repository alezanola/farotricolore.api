﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FaroTricolore.Api.Shared.ValueObjects.Base
{
    public abstract class ValueObjectString<T> where T : ValueObjectString<T>
    {
        private string Value { get; }

        protected ValueObjectString(string value)
        {
            this.Value = value;
        }

        public override bool Equals(object other)
        {
            return this.Equals(other as T);
        }

        protected virtual IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>();
        }

        public bool Equals(T other)
        {
            if (other == null) return false;

            return
                this.GetAttributesToIncludeInEqualityCheck()
                    .SequenceEqual(other.GetAttributesToIncludeInEqualityCheck());
        }

        public static bool operator ==(ValueObjectString<T> left, ValueObjectString<T> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ValueObjectString<T> left, ValueObjectString<T> right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            return this.GetAttributesToIncludeInEqualityCheck()
                .Aggregate(17, (current, obj) => current * 31 + (obj?.GetHashCode() ?? 0));
        }

        public virtual string GetValue()
        {
            return this.Value;
        }

        public virtual bool IsValid()
        {
            return this.Value != null;
        }

        public virtual bool IsValidWithLengthLimit(int lengthLimit)
        {
            return this.Value.Length <= lengthLimit;    
        }

        public virtual void ChkIsValid(string message = "")
        {
            if (this.IsValid())
                return;

            if (string.IsNullOrEmpty(message))
                message = $"{GetType().FullName} is Required!";
            throw new ArgumentNullException(typeof(T).ToString(), message);
        }

        public virtual void ChkIsValidWithLengthLimit(int limit, string message = "")
        {
            if (this.IsValid() && this.IsValidWithLengthLimit(limit))
                return;

            if (string.IsNullOrEmpty(message))
                message = $"{GetType().FullName} exceeds the length limit!";
            throw new ArgumentNullException(typeof(T).ToString(), message);
        }
    }
}
