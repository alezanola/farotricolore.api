﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FaroTricolore.Api.Shared.ValueObjects.Base
{
    public abstract class ValueObjectFloatGreaterThanZero<T> where T : ValueObjectFloatGreaterThanZero<T>
    {
        public float Value { get; }

        protected ValueObjectFloatGreaterThanZero(float value)
        {
            this.Value = value;
        }

        public override bool Equals(object other)
        {
            return this.Equals(other as T);
        }

        protected virtual IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<object>();
        }

        public bool Equals(T other)
        {
            if (other == null) return false;

            return
                this.GetAttributesToIncludeInEqualityCheck()
                    .SequenceEqual(other.GetAttributesToIncludeInEqualityCheck());
        }

        public static bool operator ==(ValueObjectFloatGreaterThanZero<T> left, ValueObjectFloatGreaterThanZero<T> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ValueObjectFloatGreaterThanZero<T> left, ValueObjectFloatGreaterThanZero<T> right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            return this.GetAttributesToIncludeInEqualityCheck()
                .Aggregate(17, (current, obj) => current * 31 + (obj?.GetHashCode() ?? 0));
        }

        public virtual float GetValue()
        {
            return this.Value;
        }

        public virtual bool IsValid()
        {
            return this.Value > 0;
        }

        public virtual void ChkIsValid(string message = "")
        {
            if (this.IsValid())
                return;

            if (string.IsNullOrEmpty(message))
                message = $"{GetType().FullName} is Required!";
            throw new ArgumentNullException(GetType().FullName, message);
        }
    }
}