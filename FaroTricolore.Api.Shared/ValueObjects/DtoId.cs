using System;

namespace FaroTricolore.Api.Shared.ValueObjects
{
    public class DtoId : DomainIdBase<DtoId>
    {
        public DtoId(Guid value) : base(value)
        {
        }
    }
}