using System.Collections.Generic;
using System.Threading.Tasks;
using FaroTricolore.Api.Shared.Models;

namespace FaroTricolore.Api.Shared.Abstracts.ReadServices
{
    public interface IMediaReadService
    {
        Task<IEnumerable<EventWithFirstImageJson>> GetRecentEventsImage(int? qty = null, int? page = null);
        Task<EventMediaJson> GetEventMedia(string eventId);
    }
}