using System.Collections.Generic;
using System.Threading.Tasks;
using FaroTricolore.Api.Shared.Models;

namespace FaroTricolore.Api.Shared.Abstracts.ReadServices
{
    public interface IEventReadService
    {
        Task<IEnumerable<EventLiteResponseJson>> GetFutureEvents(int? qty = null, int? page = null);
        Task<IEnumerable<EventLiteResponseJson>> GetPastEvents(int? qty = null, int? page = null);
        Task<IEnumerable<EventLiteResponseJson>> GetAllEvents(int? qty = null, int? page = null);
        Task<EventDetailedJson> GetEventById(string id);
    }
}