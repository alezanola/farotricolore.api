using System.Collections.Generic;
using System.Threading.Tasks;
using FaroTricolore.Api.Shared.Models;

namespace FaroTricolore.Api.Shared.Abstracts.Services
{
    public interface IUserService
    {
        Task<UserInfoJson> AuthenticateAsync(UserCredentialsJson userCredentialsJson);

        Task CreateUserAsync(UserJson userJson);
        Task UpdateUserAsync(UserJson userJson);
        Task<IEnumerable<UserJson>> GetAllUsersAsync();
        Task<UserJson> GetUserByIdAsync(string id);
        Task DeleteUserAsync(string id);
    }
}