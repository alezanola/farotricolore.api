using System.Threading.Tasks;

namespace FaroTricolore.Api.Shared.Abstracts.Services
{
    public interface IMigrationService
    {
        Task MigrateImages();
    }
}