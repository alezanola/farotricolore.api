using System.Collections.Generic;
using System.Threading.Tasks;
using FaroTricolore.Api.Shared.Models;

namespace FaroTricolore.Api.Shared.Abstracts.Services
{
    public interface IMediaService
    {
        Task InsertMediaAsync(string id, string posterBase64);
        Task UpdateMediaAsync(string id, string base64);
        Task RemovePoster(string id);
        Task DeleteMediaAsync(string id);
        Task AddImagesAsync(string mediaId, string[] imagesToAddJson);
        Task RemoveImageAsync(string id, string imageId);
        Task OrderImagesAsync(string id, string imageId, int newPosition);
        Task EditVideo(string eventId, IEnumerable<VideoJson> video);
    }
}