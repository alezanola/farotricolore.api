﻿using System.Threading.Tasks;
using FaroTricolore.Api.Shared.Models;

namespace FaroTricolore.Api.Shared.Abstracts.Services
{
    public interface IEventService
    {
        Task InsertEventAsync(EventToAddJson eventToAddJson);
        Task UpdateEventAsync(EventToAddJson eventToAddJson);
        Task DeleteEventAsync(string id);
    }
}
