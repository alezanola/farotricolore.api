# Use Microsoft's official build .NET image.
# https://hub.docker.com/_/microsoft-dotnet-core-sdk/
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build

# Install production dependencies.
# Copy csproj and restore as distinct layers.
WORKDIR /src
COPY *.sln ./
COPY FaroTricolore.Api/*.csproj ./FaroTricolore.Api/
COPY FaroTricolore.Api.Mediator/*.csproj ./FaroTricolore.Api.Mediator/
COPY FaroTricolore.Api.MongoDb/*.csproj ./FaroTricolore.Api.MongoDb/
COPY FaroTricolore.Api.Services/*.csproj ./FaroTricolore.Api.Services/
COPY FaroTricolore.Api.Shared/*.csproj ./FaroTricolore.Api.Shared/

COPY NuGet.config ./
RUN dotnet restore
COPY . ./

# Building each project
WORKDIR /src/FaroTricolore.Api.Mediator
RUN dotnet build -c Release -o /app

WORKDIR /src/FaroTricolore.Api.MongoDb
RUN dotnet build -c Release -o /app

WORKDIR /src/FaroTricolore.Api.Services
RUN dotnet build -c Release -o /app

WORKDIR /src/FaroTricolore.Api.Shared
RUN dotnet build -c Release -o /app

WORKDIR /src/FaroTricolore.Api
RUN dotnet build -c Release -o /app
# Publish
RUN dotnet publish -c Release -o /app

# Use Microsoft's official runtime .NET image.
# https://hub.docker.com/_/microsoft-dotnet-core-aspnet/
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
COPY --from=build /app ./

# Run the web service on container startup.
ENTRYPOINT ["dotnet", "FaroTricolore.Api.dll"]
