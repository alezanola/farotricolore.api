# Faro Tricolore association

"Associazione Culturale Faro Tricolore" is an italian non-profit assiciation whose goal is to divulge Risorgimento history through public events, books presentations and much more.

Visit the [website](https://farotricolore.it/) to learn more!

## Api

This project is the web service that makes the website works. This Api has been developed using ASP.Net Core 3.1 and MongoDB.

It is automatically published by GitLab CI pipeline to the Cloud Run container service of the Google Cloud Platform.
