using FaroTricolore.Api.Shared.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Simple.Shared.Abstracts;
using Simple.Shared.Services;
using CompressionQuality = Simple.Shared.Configurations.CompressionQuality;

namespace FaroTricolore.Api.Services.Services
{
    public class ImageService : ImageAbstractService
    {
        public ImageService(IOptions<AppSettings> settings, ILogger<ImageAbstractService> logger, IStorageService storageService) :
            base(settings.Value.Storage.ToShared(), logger, storageService, settings.Value.CompressionQuality.ToShared())
        {
        }
    }
}