using FaroTricolore.Api.Shared.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Simple.Shared.Configurations;
using Simple.Shared.Services;

namespace FaroTricolore.Api.Services.Services
{
    public class GoogleStorageService : GoogleStorageAbstractService
    {
        public GoogleStorageService(IOptions<AppSettings> settings, ILogger<GoogleStorageAbstractService> logger) : 
            base(settings.Value.Storage.ToShared(), logger)
        {
        }
    }
}