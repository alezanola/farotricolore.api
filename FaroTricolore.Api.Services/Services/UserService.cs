using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.MongoDb.Factories;
using FaroTricolore.Api.Shared.Abstracts;
using FaroTricolore.Api.Shared.Abstracts.Services;
using FaroTricolore.Api.Shared.Configuration;
using FaroTricolore.Api.Shared.Models;
using FaroTricolore.Api.Shared.ValueObjects;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using EventId = Microsoft.Extensions.Logging.EventId;

namespace FaroTricolore.Api.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IDocumentUnitOfWork _documentUnitOfWork;
        private readonly AppSettings _settings;
        private readonly ILogger _logger;

        private readonly byte[] _secretKey;
        
        public UserService(IDocumentUnitOfWork documentUnitOfWork, IOptions<AppSettings> settings,
            ILogger<UserService> logger)
        {
            _documentUnitOfWork = documentUnitOfWork;
            _settings = settings.Value;
            _logger = logger;
            
            var keyString = Environment.GetEnvironmentVariable(_settings.TokenAuthentication.SecretKeyVariableName) 
                ?? ""; // Never reach this because check is done at startup (in absent, throws an exception and stops)
            _secretKey = Encoding.ASCII.GetBytes(keyString);
        }
        
        public async Task<UserInfoJson> AuthenticateAsync(UserCredentialsJson userCredentialsJson)
        {
            var users = await _documentUnitOfWork.UserRepository.FindAsync(x => true);
            if (users.Count == 0)
            {
                await CreateDefaultUserAsync();
            }
            
            var userResult = await _documentUnitOfWork.UserRepository.FindAsync(x => 
                x.Email == userCredentialsJson.Email && 
                x.Password == new Password(userCredentialsJson.Password).GetValue()
                );

            var user = userResult.FirstOrDefault();
            if(user == null)
                throw new Exception("Invalid credentials!");

            var claims = new[]
            {
                new Claim(ClaimTypes.Name, user.Id),
                new Claim(ClaimTypes.Role, user.Role)
            };

            var now = DateTime.Now;
            var expirationDate = now.AddHours(this._settings.TokenAuthentication.TokenExpirationHours);
            
            var jwt = new JwtSecurityToken(
                issuer: this._settings.TokenAuthentication.Issuer,
                audience: this._settings.TokenAuthentication.Audience,
                claims: claims,
                notBefore: now,
                expires: expirationDate,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(_secretKey),
                    SecurityAlgorithms.HmacSha256
                    ));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var userInfo = new UserInfoJson()
            {
                Email = user.Email,
                Name = user.Name,
                Surname = user.Surname,
                Role = user.Role,
                Token = encodedJwt,
                TokenExpiration = expirationDate
            };

            return userInfo;
        }

        private async Task CreateDefaultUserAsync()
        {
            var defaultUser = new UserJson
            {
                Email = "ales.zanola@gmail.com",
                Password = "admin",
                Name = "Alessandro",
                Surname = "Zanola",
                Role = "Admin"
            };
            await CreateUserAsync(defaultUser);
        }


        public async Task CreateUserAsync(UserJson userJson)
        {
            try
            {
                if (!Roles.Exists(userJson.Role))
                    throw new Exception("Il ruolo inserito non esiste!");
                var noSqlUser = UserDtoFactory.CreateUserDtoFromJson(userJson);
                await _documentUnitOfWork.UserRepository.InsertOneAsync(noSqlUser);
            }
            catch (Exception ex)
            {
                _logger.LogError(new EventId(1, "UserService.GetAllUsersAsync"),
                    ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                throw;
            }
        }

        public async Task UpdateUserAsync(UserJson userJson)
        {
            try
            {
                if (userJson.Password == "")
                {
                    var oldUser = await _documentUnitOfWork.UserRepository.GetByIdAsync(userJson.Id);
                    userJson.Password = oldUser.Password;
                }
                var userDto = UserDtoFactory.CreateUserDtoFromJson(userJson);

                await _documentUnitOfWork.UserRepository.SaveAsync(userDto);
            }
            catch (Exception ex)
            {
                _logger.LogError(new EventId(1, "UserService.UpdateUserAsync"),
                    ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                throw;
            }
        }

        public async Task<IEnumerable<UserJson>> GetAllUsersAsync()
        {
            try
            {
                var noSqlUsers = await _documentUnitOfWork.UserRepository.FindAsync(x => true);
                var userJsons = noSqlUsers.Select(x => x.ToJson());
                return userJsons;
            }
            catch (Exception ex)
            {
                _logger.LogError(new EventId(1, "UserService.GetAllUsersAsync"),
                    ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                throw;
            }
        }

        public async Task<UserJson> GetUserByIdAsync(string id)
        {
            try
            {
                var noSqlUser = await _documentUnitOfWork.UserRepository.FindAsync(x => x.Id == id);
            
                var user = noSqlUser.FirstOrDefault();
                if (user == null) 
                    throw new Exception("User not found");
            
                return user.ToJson();
            }
            catch (Exception ex)
            {
                _logger.LogError(new EventId(1, "UserService.GetAllUsersAsync"),
                    ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                throw;
            }
        }

        public async Task DeleteUserAsync(string id)
        {
            try
            {
                await _documentUnitOfWork.UserRepository.DeleteOneByIdAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(new EventId(1, "UserService.GetAllUsersAsync"),
                    ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                throw;
            }
        }
    }
}