using System;
using System.Linq;
using System.Threading.Tasks;
using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.MongoDb.Documents;
using FaroTricolore.Api.Shared.Abstracts.Services;
using Microsoft.Extensions.Logging;
using Simple.Shared;
using Simple.Shared.Abstracts;
using Simple.Shared.Models;

namespace FaroTricolore.Api.Services.Services
{
    public class MigrationService : IMigrationService
    {
        private readonly IDocumentUnitOfWork _unitOfWork;
        private readonly IStorageService _storageService;
        private readonly IImageService _imageService;
        private readonly ILogger<MigrationService> _logger;

        public MigrationService(IDocumentUnitOfWork unitOfWork, IStorageService storageService, IImageService imageService, ILogger<MigrationService> logger)
        {
            _unitOfWork = unitOfWork;
            _storageService = storageService;
            _imageService = imageService;
            _logger = logger;
        }

        public async Task MigrateImages()
        {
            var media = await _unitOfWork.MediaRepository.FindAsync();
            foreach (var mediaDto in media)
            {
                try
                {
                    _unitOfWork.MediaRepository.StartTransaction();
                    _storageService.BeginTransaction();
                    
                    _logger.LogInformation("Processing: " + mediaDto.Id);
                    await MigrateMediaDto(mediaDto);
                    await _unitOfWork.MediaRepository.SaveAsync(mediaDto);
                    
                    _logger.LogInformation("Saving changes: " + mediaDto.Id);
                    await _storageService.CommitAsync();
                    await _unitOfWork.MediaRepository.CommitTransactionAsync();
                    _logger.LogInformation("Completed: " + mediaDto.Id);
                }
                catch (Exception e)
                {
                    await _storageService.RollbackAsync();
                    await _unitOfWork.MediaRepository.AbortTransactionAsync();
                    CommonServices.LogDefaultExceptionErrors(e, _logger, this);
                    _logger.LogError("Errore media id: " + mediaDto.Id);
                }
            }
        }

        private async Task MigrateMediaDto(MediaDto dto)
        {
            var basePath = RemoveSlash(dto.EventPath);
            var tasks = dto.Images?.Select(eventImage => 
                MigrateEventImage(eventImage, basePath + "images/"));
            
            var posterFile = await _storageService.DownloadFile(basePath + dto.PosterName);

            var image = await _imageService.CreateImageModel(posterFile, GetExtension(dto.PosterName));
            var uploadTask = _imageService.UploadImage(image);
            dto.Poster = CreateEventImage(image);

            await uploadTask;
            if(tasks != null)
                await Task.WhenAll(tasks);
        }

        private async Task MigrateEventImage(EventImageDto eventImageDto, string basePath)
        {
            var imageFile = await _storageService.DownloadFile(basePath + eventImageDto.Name);

            var image = await _imageService.CreateImageModel(imageFile, GetExtension(eventImageDto.Name));
            var uploadTask = _imageService.UploadImage(image);

            eventImageDto.Large = image.GetPathLg();
            eventImageDto.Medium = image.GetPathMd();
            eventImageDto.Small = image.GetPathSm();

            await uploadTask;
        }

        
        
        private static EventImageDto CreateEventImage(ImageCompressed image)
        {
            return new EventImageDto
            {
                Large = image.GetPathLg(),
                Medium = image.GetPathMd(),
                Small = image.GetPathSm()
            };
        }
        
        private static string GetExtension(string filename)
        {
            return filename.Split('.').Last();
        }

        private static string RemoveSlash(string path)
        {
            return path.Substring(1);
        }
    }
}