﻿using System;
using System.Threading.Tasks;
using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.MongoDb.Documents;
using FaroTricolore.Api.Shared.Abstracts.Services;
using FaroTricolore.Api.Shared.Configuration;
using FaroTricolore.Api.Shared.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using EventIdLogger = Microsoft.Extensions.Logging.EventId;

namespace FaroTricolore.Api.Services.Services
{
    public class EventService : IEventService
    {
        private readonly IDocumentUnitOfWork _unitOfWork;
        private readonly ILogger<EventService> _logger;
        private readonly IMediaService _mediaService;
        private readonly AppSettings _settings;
        
        public EventService(IDocumentUnitOfWork unitOfWork,
            ILogger<EventService> logger,
            IMediaService mediaService, IOptions<AppSettings> settings)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mediaService = mediaService;
            _settings = settings.Value;
        }
        

        public async Task InsertEventAsync(EventToAddJson eventToAddJson)
        {
            try
            {
                var id = Guid.NewGuid();

                var existingId = await _unitOfWork.EventRepository.GetByIdAsync(id.ToString("N"));
                if(existingId != null)
                    throw new Exception("Id already exist. Try again!");
                
                var eventDto = EventDto.CreateNoSqlEventFromJson(eventToAddJson, id);
                Task insertMediaToDbTask = null;
                if(eventToAddJson.PosterBase64 != null) 
                    insertMediaToDbTask = _mediaService.InsertMediaAsync(eventDto.Id, eventToAddJson.PosterBase64);
                await _unitOfWork.EventRepository.InsertOneAsync(eventDto);
                if (insertMediaToDbTask != null)
                    await insertMediaToDbTask;

                await _unitOfWork.EventRepository.CommitTransactionAsync();
            }
            catch (Exception ex)
            {
                await _unitOfWork.EventRepository.AbortTransactionAsync();
                this._logger.LogError(new EventIdLogger(1, "EventsService.InsertEventAsync"),
                    ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                throw;
            }
        }

        public async Task UpdateEventAsync(EventToAddJson eventToAddJson)
        {
            try
            {
                var evt = EventDto.CreateNoSqlEventFromJson(eventToAddJson);
                var saveEventTask = _unitOfWork.EventRepository.SaveAsync(evt);

                if (eventToAddJson.RemovePoster ?? false)
                    await _mediaService.RemovePoster(eventToAddJson.Id);
                else if (!string.IsNullOrWhiteSpace(eventToAddJson.PosterBase64))
                    await _mediaService.UpdateMediaAsync(eventToAddJson.Id, eventToAddJson.PosterBase64);

                await saveEventTask;
            }
            catch (Exception ex)
            {
                this._logger.LogError(new EventIdLogger(1, "EventsService.UpdateEventAsync"),
                    ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                throw;
            }
        }

        public async Task DeleteEventAsync(string id)
        {
            try
            {
                var evt = await _unitOfWork.EventRepository.GetByIdAsync(id);
                if(evt == null)
                    throw new Exception("The event does not exist!");
                var deleteMediaTask = _mediaService.DeleteMediaAsync(id);
                await _unitOfWork.EventRepository.DeleteOneByIdAsync(id);
                await deleteMediaTask;
            }
            catch (Exception ex)
            {
                this._logger.LogError(new EventIdLogger(1, "EventsService.DeleteEventAsync"),
                    ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                throw;
            }
        }
    }
}
