using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.MongoDb.Documents;
using FaroTricolore.Api.MongoDb.Factories;
using FaroTricolore.Api.Shared.Abstracts.Services;
using FaroTricolore.Api.Shared.Configuration;
using FaroTricolore.Api.Shared.Models;
using FaroTricolore.Api.Shared.ValueObjects;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Simple.Shared;
using Simple.Shared.Abstracts;

namespace FaroTricolore.Api.Services.Services
{
    public class MediaService : IMediaService
    {
        private readonly IDocumentUnitOfWork _unitOfWork;
        private readonly IImageService _imageService;
        private readonly AppSettings _settings;
        private readonly ILogger<EventService> _logger;
        
        public MediaService(IDocumentUnitOfWork unitOfWork,
            IOptions<AppSettings> options,
            ILogger<EventService> logger, IImageService imageService)
        {
            _unitOfWork = unitOfWork;
            _settings = options.Value;
            _logger = logger;
            _imageService = imageService;
        }



        private async Task<EventImageDto> CreateEventImage(string base64)
        {
            var image = await _imageService.CreateImageModel(base64);
            await _imageService.UploadImage(image);

            return new EventImageDto
            {
                Id = image.GetGuid(),
                Large = image.GetPathLg(),
                Medium = image.GetPathMd(),
                Small = image.GetPathSm()
            };
        }

        private async Task DeleteEventImage(EventImageDto imageDto)
        {
            await _imageService.DeleteIfExists(imageDto.Large);
            await _imageService.DeleteIfExists(imageDto.Medium);
            await _imageService.DeleteIfExists(imageDto.Small);
        }
        
        
        
        public async Task InsertMediaAsync(string id, string posterBase64)
        {
            try
            {
                var mediaDto = MediaDtoFactory.CreateDto(
                    new DtoId(new Guid(id)), 
                    await CreateEventImage(posterBase64)
                );
                
                await _unitOfWork.MediaRepository.InsertOneAsync(mediaDto);
            }
            catch (Exception e)
            {
                CommonServices.LogDefaultExceptionErrors(e, _logger, this);
                throw;
            }
        }

        public async Task UpdateMediaAsync(string id, string base64)
        {
            try
            {
                var mediaDto = await _unitOfWork.MediaRepository.GetByIdAsync(id);
                if (mediaDto == null)
                {
                    await InsertMediaAsync(id, base64);
                    return;
                }

                await DeleteEventImage(mediaDto.Poster);
                mediaDto.Poster = await CreateEventImage(base64);
                await _unitOfWork.MediaRepository.SaveAsync(mediaDto);
            }
            catch (Exception e)
            {
                await _unitOfWork.MediaRepository.AbortTransactionAsync();
                CommonServices.LogDefaultExceptionErrors(e, _logger, this);
                throw;
            }
        }

        public async Task RemovePoster(string id)
        {
            try
            {
                var mediaDto = await _unitOfWork.MediaRepository.GetByIdAsync(id);
                if (mediaDto?.Poster == null) 
                    return;

                await DeleteEventImage(mediaDto.Poster);
                mediaDto.Poster = null;
                await _unitOfWork.MediaRepository.SaveAsync(mediaDto);
            }
            catch (Exception e)
            {
                await _unitOfWork.MediaRepository.AbortTransactionAsync();
                CommonServices.LogDefaultExceptionErrors(e, _logger, this);
                throw;
            }
        }

        public async Task DeleteMediaAsync(string id)
        {
            try
            {
                var mediaDto = await _unitOfWork.MediaRepository.GetByIdAsync(id);
                if(mediaDto == null)
                    return;
                
                if(mediaDto.Images != null && mediaDto.Images.Any())
                    throw new Exception("Can't delete it because some images still exist.");

                await DeleteEventImage(mediaDto.Poster);
                await Task.WhenAll(mediaDto.Images?.Select(DeleteEventImage));
                
                await _unitOfWork.MediaRepository.DeleteOneByIdAsync(id);
            }
            catch (Exception ex)
            {
                CommonServices.LogDefaultExceptionErrors(ex, _logger, this);
                throw;
            }
        }

        public async Task AddImagesAsync(string mediaId, string[] imagesToAddJson)
        {
            try
            {
                var media = await _unitOfWork.MediaRepository.GetByIdAsync(mediaId) ?? MediaDtoFactory.CreateDto(
                    new DtoId(new Guid(mediaId)), 
                    new EventImageDto()
                );

                var imagesToAdd = await Task.WhenAll(
                    imagesToAddJson.Select(async t => await CreateEventImage(t)));

                var images = media.Images?.ToList() ?? new List<EventImageDto>();
                images.AddRange(imagesToAdd);
                media.Images = images;

                await _unitOfWork.MediaRepository.SaveAsync(media);
            }
            catch (Exception e)
            {
                await _unitOfWork.MediaRepository.AbortTransactionAsync();
                CommonServices.LogDefaultExceptionErrors(e, _logger, this);
                throw;
            }
        }

        public async Task RemoveImageAsync(string id, string imageId)
        {
            try
            {
                var media = await _unitOfWork.MediaRepository.GetByIdAsync(id);
                if(media == null) return;

                var images = media.Images.ToList();
                var imageRemoved = images.First(t => t.Id == imageId);
                images.RemoveAll(t => t.Id == imageId);
                
                media.Images = images;
                var saveToDbTask = _unitOfWork.MediaRepository.SaveAsync(media);

                await DeleteEventImage(imageRemoved);

                await saveToDbTask;
            }
            catch (Exception ex)
            {
                CommonServices.LogDefaultExceptionErrors(ex, _logger, this);
                throw;
            }
        }

        public async Task OrderImagesAsync(string id, string imageId, int newPosition)
        {
            try
            {
                var media = await _unitOfWork.MediaRepository.GetByIdAsync(id);
                if(media == null) return;

                var images = media.Images.ToList();
                var imgToMove = images.Find(t => t.Id == imageId);
                if(imgToMove == null) return;
                images.RemoveAll(t => t.Id == imageId);

                var newImages = images.GetRange(0, newPosition);
                newImages.Add(imgToMove);
                newImages.AddRange(images.GetRange(newPosition, images.Count - newPosition));

                media.Images = newImages;
                await _unitOfWork.MediaRepository.SaveAsync(media);
            }
            catch (Exception ex)
            {
                CommonServices.LogDefaultExceptionErrors(ex, _logger, this);
                throw;
            }
        }

        public async Task EditVideo(string eventId, IEnumerable<VideoJson> videos)
        {
            var mediaDto = await _unitOfWork.MediaRepository.GetByIdAsync(eventId);
            if (mediaDto == null)
            {
                var eventDto = await _unitOfWork.EventRepository.GetByIdAsync(eventId);
                if(eventDto == null)
                    throw new Exception("Evento non trovato");
                mediaDto = new MediaDto
                {
                    Id = eventId
                };
            }
            mediaDto.Videos = videos.Select(t => t.ToDto());
            await _unitOfWork.MediaRepository.SaveAsync(mediaDto);
        }
    }
}