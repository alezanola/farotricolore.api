using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.MongoDb.Factories;
using FaroTricolore.Api.Shared.Abstracts.ReadServices;
using FaroTricolore.Api.Shared.Configuration;
using FaroTricolore.Api.Shared.Models;
using Microsoft.Extensions.Options;

namespace FaroTricolore.Api.Services.ReadServices
{
    public class MediaReadService : IMediaReadService
    {
        private readonly IDocumentUnitOfWork _unitOfWork;
        private readonly AppSettings _settings;

        public MediaReadService(IDocumentUnitOfWork unitOfWork, IOptions<AppSettings> settings)
        {
            _unitOfWork = unitOfWork;
            _settings = settings.Value;
        }

        public async Task<IEnumerable<EventWithFirstImageJson>> GetRecentEventsImage(int? qty = null, int? page = null)
        {
            var events = await _unitOfWork.EventRepository.FindAsync(
                t => !t.HideEvent, t => t.StartDate, true, qty, page);

            var eventsJson = events.Select(async eventDto =>
            {
                var media = await _unitOfWork.MediaRepository.GetByIdAsync(eventDto.Id);
                return media?.Images?.Any() ?? false
                    ? eventDto.ToEventWithFirstImageJson(media, _settings.Storage.StorageBasePath)
                    : null;
            });

            return (await Task.WhenAll(eventsJson)).Where(t => t != null);
            
            // var mediaDtos = await _unitOfWork.MediaRepository.FindAsync(
            //     t => t.Images != null && t.Images.Any(), 
            //     qty: qty, page: page);
            //
            // var eventsTask = mediaDtos
            //     .Select(async media =>
            // {
            //     var eventDto = await _unitOfWork.EventRepository.GetByIdAsync(media.Id);
            //     return eventDto.HideEvent ? null
            //         : eventDto.ToEventWithFirstImageJson(media, _settings.Storage.StorageBasePath);
            // });
            // return (await Task.WhenAll(eventsTask)).Where(t => t != null)
            //     .OrderByDescending(t => t.StartDate);
        }

        public async Task<EventMediaJson> GetEventMedia(string eventId)
        {
            var eventDto = await _unitOfWork.EventRepository.GetByIdAsync(eventId);
            if(eventDto == null)
                throw new Exception("Evento non trovato!");
            var mediaDto = await _unitOfWork.MediaRepository.GetByIdAsync(eventId);
            if(mediaDto == null)
                return new EventMediaJson
                {
                    Id = eventDto.Id,
                    Name = eventDto.Name
                };

            var json = mediaDto.ToJson(eventDto, _settings.Storage.StorageBasePath);

            return json;
        }
    }
}