using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.MongoDb.Documents;
using FaroTricolore.Api.MongoDb.Factories;
using FaroTricolore.Api.Shared.Abstracts.ReadServices;
using FaroTricolore.Api.Shared.Configuration;
using FaroTricolore.Api.Shared.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Simple.Shared;

namespace FaroTricolore.Api.Services.ReadServices
{
    public class EventReadService : IEventReadService
    {
        private readonly IDocumentUnitOfWork _unitOfWork;
        private readonly ILogger<EventReadService> _logger;
        private readonly AppSettings _settings;

        public EventReadService(IDocumentUnitOfWork unitOfWork, ILogger<EventReadService> logger, IOptions<AppSettings> settings)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _settings = settings.Value;
        }

        public async Task<IEnumerable<EventLiteResponseJson>> GetFutureEvents(int? qty = null, int? page = null)
        {
            try
            {
                var noSqlEvents =
                    await _unitOfWork.EventRepository.FindAsync(
                        t => t.StartDate > DateTime.UtcNow && !t.HideEvent, 
                        t => t.StartDate, qty: qty, page: page);

                var eventLiteResponseJsons = noSqlEvents.Select(
                    noSqlEvent => noSqlEvent.ToLiteJson()).ToList();
                
                if (!eventLiteResponseJsons.Any())
                    return Enumerable.Empty<EventLiteResponseJson>();

                // To show in modal
                var firstEvent = eventLiteResponseJsons.First();
                var media = await _unitOfWork.MediaRepository.GetByIdAsync(firstEvent.Id);
                firstEvent.Poster = media?.Poster?.ToJson(_settings.Storage.StorageBasePath);

                return eventLiteResponseJsons;

            }
            catch (Exception ex)
            {
                CommonServices.LogDefaultExceptionErrors(ex, _logger, this);
                throw;
            }
        }

        public async Task<IEnumerable<EventLiteResponseJson>> GetPastEvents(int? qty = null, int? page = null)
        {
            try
            {
                var noSqlEvents =
                    await _unitOfWork.EventRepository.FindAsync(
                        t => t.StartDate <= DateTime.UtcNow && !t.HideEvent, 
                        t => t.StartDate, 
                        true, qty, page);

                var eventsTask = noSqlEvents.Select(async noSqlEvent =>
                {
                    var media = await _unitOfWork.MediaRepository.GetByIdAsync(noSqlEvent.Id);
                    var eventJson = noSqlEvent.ToLiteJson(media, _settings.Storage.StorageBasePath);
                    
                    return eventJson;
                });

                return await Task.WhenAll(eventsTask);
            }
            catch (Exception ex)
            {
                CommonServices.LogDefaultExceptionErrors(ex, _logger, this);
                throw;
            }
        }

        public async Task<IEnumerable<EventLiteResponseJson>> GetAllEvents(int? qty = null, int? page = null)
        {
            try
            {
                var noSqlEvents =
                    await _unitOfWork.EventRepository.FindAsync(
                        t => true, 
                        t => t.StartDate, 
                        true, qty, page);

                return noSqlEvents.Select(noSqlEvent =>
                {
                    var eventJson = noSqlEvent.ToLiteJson();
                    return eventJson;
                });
            }
            catch (Exception ex)
            {
                CommonServices.LogDefaultExceptionErrors(ex, _logger, this);
                throw;
            }
        }

        public async Task<EventDetailedJson> GetEventById(string id)
        {
            try
            {
                var evt = await _unitOfWork.EventRepository.GetByIdAsync(id);
                if(evt == null)
                    throw new Exception("Nessun evento trovato!");
                
                var media = await _unitOfWork.MediaRepository.GetByIdAsync(id);
                var eventJson = evt.ToDetailedJson(media, _settings.Storage.StorageBasePath);
                
                return eventJson;
            }
            catch (Exception ex)
            {
                CommonServices.LogDefaultExceptionErrors(ex, _logger, this);
                throw;
            }
        }
    }
}