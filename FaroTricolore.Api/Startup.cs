﻿using System;
using System.IO;
using System.Text;
using FaroTricolore.Api.Mediator;
using FaroTricolore.Api.Shared.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace FaroTricolore.Api
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostEnvironment env)
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.LiterateConsole()
                .CreateLogger();

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            this.Configuration = builder.Build();
        }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public void ConfigureServices(IServiceCollection services)
        {
            #region CORS
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", corsBuilder =>
                    corsBuilder.WithOrigins("https://farotricolore.it",
                                            "https://www.farotricolore.it",
                                            "https://web.farotricolore.it",
                                            "http://localhost:4200",
                                            "http://localhost:4000",
                                            "http://localhost:8080")
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
            #endregion

            services.AddControllers();

            #region Configuration

            var appSettingsSection = this.Configuration.GetSection("FaroTricolore");
            services.Configure<AppSettings>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettings>();
            
            #endregion

            #region Authentication

            var keyString = Environment.GetEnvironmentVariable(appSettings.TokenAuthentication.SecretKeyVariableName) 
                            ?? throw new ArgumentException("Secret key not set as environment variable! Please, check the configuration.");
            var key = Encoding.ASCII.GetBytes(keyString);
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                    x.Events = new JwtBearerEvents()
                    {
                        OnChallenge = c =>
                        {
                            c.HandleResponse();
                            
                            c.Response.StatusCode = 401;
                            c.Response.ContentType = "application/json";
                     
                            var r = new JObject
                            {
                                ["error"] = c.Error,
                                ["errorDescription"] = c.ErrorDescription
                            };
                            
                            return c.Response.WriteAsync(r.ToString());
                        }
                    };
                });

            #endregion

            #region Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "FaroTricolore API",
                    Version = "v1",
                    Description = "Web Api Services to manage the website of Faro Tricolore's association",
                    Contact = new OpenApiContact()
                    {
                        Name = "Alessandro Zanola",
                        Email = "ales.zanola@gmail.com"
                    }
                });

                var basePath = AppContext.BaseDirectory;
                var assemblyName = System.Reflection.Assembly.GetEntryAssembly()?.GetName().Name;
                var fileName = Path.GetFileName(assemblyName + ".xml");
                var filePath = Path.Combine(basePath, fileName);
                if (File.Exists(filePath))
                    c.IncludeXmlComments(filePath);
            });
            #endregion

            #region Dependency injection

            services.AddDataModel();
            services.AddServices();
            services.AddReadServices();

            #endregion
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="applicationLifetime"></param>
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime applicationLifetime)
        {
            #region Logging
            loggerFactory.AddSerilog();
            loggerFactory.AddFile("Logs/FaroTricolore-{Date}.txt");

            // Ensure any buffered events are sent at shutdown
            applicationLifetime.ApplicationStopped.Register(Log.CloseAndFlush);
            #endregion

            #region Routing
            app.UseRouting();
            #endregion

            #region CORS
            app.UseCors("CorsPolicy");
            #endregion
            
            #region Authentication
            app.UseAuthentication();
            app.UseAuthorization();
            #endregion

            #region Endpoints routing
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            #endregion

            #region Swagger
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "documentation/{documentName}/documentation.json";
            });
            app.UseSwaggerUI(c =>
            {
                //c.ShowRequestHeaders();
                c.SwaggerEndpoint("/documentation/v1/documentation.json", "Faro Tricolore Web API");
            });
            #endregion
        }
    }
}
