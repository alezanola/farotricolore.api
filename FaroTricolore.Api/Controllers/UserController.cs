using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using FaroTricolore.Api.Shared.Abstracts;
using FaroTricolore.Api.Shared.Abstracts.Services;
using FaroTricolore.Api.Shared.Configuration;
using FaroTricolore.Api.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FaroTricolore.Api.Controllers
{
    /// <summary>
    /// User controller. To log in and manage account
    /// </summary>
    [Authorize]
    [Route("v1/[Controller]")]
    public class UserController : BaseController
    {
        private readonly IUserService _usersService;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:FaroTricolore.Api.UsersController"/> class.
        /// </summary>
        public UserController(ILogger<UserController> logger, 
            IUserService usersService) : base(logger)
        {
            this._usersService = usersService;
        }

        /// <summary>
        /// Return account info from email and password
        /// </summary>
        /// <returns></returns>
        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<ActionResult<UserInfoJson>> Authenticate([FromBody] UserCredentialsJson userCredentialsJson)
        {
            try
            {
                var token = await _usersService.AuthenticateAsync(userCredentialsJson);
                return this.Ok(token);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Get all users (only admin)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = Roles.Admin)]
        public async Task<ActionResult<IEnumerable<UserJson>>> GetAllUsers()
        {
            try
            {
                var users = await _usersService.GetAllUsersAsync();
                return this.Ok(users);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Delete logged user
        /// </summary>
        /// <returns></returns>
        [HttpGet("{userId}")]
        public async Task<ActionResult<UserJson>> GetUserById([FromRoute] string userId)
        {
            try
            {
                if (User.Identity.Name != userId && !User.IsInRole(Roles.Admin))
                {
                    return Forbid();
                }
                
                var user = await _usersService.GetUserByIdAsync(userId);
                return this.Ok(user);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Create an user (only admin)
        /// </summary>
        /// <param name="userJson"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = Roles.Admin)]
        public async Task<IActionResult> CreateUser([FromBody] UserJson userJson)
        {
            try
            {
                await _usersService.CreateUserAsync(userJson);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Edit an user (each user its account and admin all)
        /// </summary>
        /// <param name="userJson"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UpdateUser([FromBody] UserJson userJson)
        {
            try
            {
                if (User.Identity.Name != userJson.Id && !User.IsInRole(Roles.Admin))
                {
                    return Forbid();
                }
                
                await _usersService.UpdateUserAsync(userJson);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Delete user by id (only admin)
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{userId}")]
        public async Task<IActionResult> DeleteUserById([FromRoute] string userId)
        {
            try
            {
                if (User.Identity.Name != userId && !User.IsInRole(Roles.Admin))
                {
                    return Forbid();
                }
                
                await _usersService.DeleteUserAsync(userId);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}