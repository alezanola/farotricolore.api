using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FaroTricolore.Api.Shared.Abstracts.ReadServices;
using FaroTricolore.Api.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FaroTricolore.Api.Controllers
{
    /// <summary></summary>
    [Route("v1/events")]
    public class EventsReadController : BaseController
    {
        private readonly IEventReadService _eventReadService;

        /// <summary></summary>
        public EventsReadController(IEventReadService eventReadService, ILogger<EventsReadController> logger) : base(logger)
        {
            _eventReadService = eventReadService;
        }

        /// <summary>
        /// Get future events in date order.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<EventLiteResponseJson>> GetFutureEvents()
        {
            try
            {
                var events = await _eventReadService.GetFutureEvents();
                return this.Ok(events);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Get future events in date order.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{qty}/{page}")]
        public async Task<ActionResult<EventLiteResponseJson>> GetFutureEvents([FromRoute] int qty, [FromRoute] int page)
        {
            try
            {
                var events = await _eventReadService.GetFutureEvents(qty, page);
                return this.Ok(events);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Gets past events in descending date order.
        /// </summary>
        /// <returns></returns>
        [HttpGet("recent")]
        public async Task<ActionResult<IEnumerable<EventLiteResponseJson>>> GetPastEvents()
        {
            try
            {
                var events = await _eventReadService.GetPastEvents();
                return this.Ok(events);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Gets past events in descending date order.
        /// </summary>
        /// <returns></returns>
        [HttpGet("recent/{qty}/{page}")]
        public async Task<ActionResult<IEnumerable<EventLiteResponseJson>>> GetPastEvents([FromRoute] int qty, [FromRoute] int page)
        {
            try
            {
                var events = await _eventReadService.GetPastEvents(qty, page);
                return this.Ok(events);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        
        /// <summary>
        /// All events in descending date order.
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        public async Task<ActionResult<EventLiteResponseJson>> GetAllEvents()
        {
            try
            {
                var events = await _eventReadService.GetAllEvents();
                return this.Ok(events);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        
        /// <summary>
        /// All events in descending date order.
        /// </summary>
        /// <returns></returns>
        [HttpGet("all/{qty}/{page}")]
        public async Task<ActionResult<EventLiteResponseJson>> GetAllEvents([FromRoute] int qty, [FromRoute] int page)
        {
            try
            {
                var events = await _eventReadService.GetAllEvents(qty, page);
                return this.Ok(events);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        


        /// <summary>
        /// Get event details by id.
        /// </summary>
        /// <param name="eventId">The event id</param>
        /// <returns></returns>
        [HttpGet("{eventId}")]
        public async Task<ActionResult<EventDetailedJson>> GetEventById([FromRoute] string eventId)
        {
            try
            {
                var evt = await _eventReadService.GetEventById(eventId);
                return this.Ok(evt);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}