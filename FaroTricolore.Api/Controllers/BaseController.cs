﻿using System;
using FaroTricolore.Api.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Simple.Shared;

namespace FaroTricolore.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Common Controller to share functions/methods
    /// </summary>
    public class BaseController : Controller
    {
        private readonly ILogger<BaseController> _logger;

        /// <summary></summary>
        public BaseController(ILogger<BaseController> logger)
        {
            _logger = logger;
        }

        internal ActionResult AnswerWithError(Exception ex)
        {
            CommonServices.LogDefaultExceptionErrors(ex, _logger, this);
            return BadRequest(new ErrorJson
            {
                Error = ex.Message
            });
        }
    }
}