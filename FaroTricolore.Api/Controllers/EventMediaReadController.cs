using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FaroTricolore.Api.Shared.Abstracts.ReadServices;
using FaroTricolore.Api.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Simple.Shared;

namespace FaroTricolore.Api.Controllers
{
    /// <summary></summary>
    [Route("v1/events/media")]
    public class EventMediaReadController : BaseController
    {
        private readonly IMediaReadService _mediaReadService;

        /// <summary></summary>
        public EventMediaReadController(IMediaReadService mediaReadService, ILogger<BaseController> logger) : base(logger)
        {
            _mediaReadService = mediaReadService;
        }

        /// <summary>
        /// Get recent events with the first image
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EventWithFirstImageJson>>> GetRecentEventsImage()
        {
            try
            {
                var events = await _mediaReadService.GetRecentEventsImage();
                return this.Ok(events);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Get recent events with the first image
        /// </summary>
        /// <returns></returns>
        [HttpGet("{qty}/{page}")]
        public async Task<ActionResult<IEnumerable<EventWithFirstImageJson>>> GetRecentEventsImage([FromRoute] int qty, [FromRoute] int page)
        {
            try
            {
                var events = await _mediaReadService.GetRecentEventsImage(qty, page);
                return this.Ok(events);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        
        /// <summary>
        /// Get all images of an event
        /// </summary>
        /// <returns></returns>
        [HttpGet("{eventId}")]
        public async Task<ActionResult<EventMediaJson>> GetEventMedia([FromRoute] string eventId)
        {
            try
            {
                var images = await _mediaReadService.GetEventMedia(eventId);
                return this.Ok(images);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}