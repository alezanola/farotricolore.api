using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FaroTricolore.Api.Shared.Abstracts.Services;
using FaroTricolore.Api.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FaroTricolore.Api.Controllers
{
    /// <summary></summary>
    [Authorize(Roles = "Admin")]
    [Route("v1/events/media")]
    public class EventMediaController : BaseController
    {
        private readonly IMediaService _mediaService;

        /// <summary></summary>
        public EventMediaController(ILogger<EventsController> logger, IMediaService mediaService) : base(logger)
        {
            _mediaService = mediaService;
        }
        


        /// <summary>
        /// Add multiple images of an event
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="imagesToAddJson"></param>
        /// <returns></returns>
        [HttpPost("{eventId}/images")]
        public async Task<IActionResult> AddImages([FromRoute] string eventId, [FromBody] string[] imagesToAddJson)
        {
            try
            {
                await _mediaService.AddImagesAsync(eventId, imagesToAddJson);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Add an image related to an event
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="imageId"></param>
        /// <returns></returns>
        [HttpDelete("{eventId}/{imageId}")]
        public async Task<IActionResult> RemoveEventImage([FromRoute] string eventId, [FromRoute] string imageId)
        {
            try
            {
                await _mediaService.RemoveImageAsync(eventId, imageId);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        // /// <summary>
        // /// Change position of an image
        // /// </summary>
        // /// <returns></returns>
        // [HttpPut("{imageId}/{newPosition}")]
        // public async Task<IActionResult> MoveEventImage([FromRoute] string id, string imageId, int newPosition)
        // {
        //     try
        //     {
        //         await _mediaService.OrderImagesAsync(id, imageId, newPosition);
        //         return this.Ok();
        //     }
        //     catch (Exception ex)
        //     {
        //         return AnswerWithError(ex);
        //     }
        // }

        /// <summary>
        /// Add a video
        /// </summary>
        /// <returns></returns>
        [HttpPost("{eventId}/videos")]
        public async Task<IActionResult> EditVideos([FromRoute] string eventId, [FromBody] IEnumerable<VideoJson> videos)
        {
            try
            {
                await _mediaService.EditVideo(eventId, videos);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}