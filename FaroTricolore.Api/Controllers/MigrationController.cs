using System;
using System.Threading.Tasks;
using FaroTricolore.Api.Shared.Abstracts.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Simple.Shared;

namespace FaroTricolore.Api.Controllers
{
    /// <summary></summary>
    [Route("v1/migration")]
    public class MigrationController : BaseController
    {
        private readonly IMigrationService _migrationService;
        private readonly ILogger<MigrationController> _logger;

        /// <summary></summary>
        public MigrationController(IMigrationService migrationService, ILogger<MigrationController> logger) : base(logger)
        {
            _migrationService = migrationService;
            _logger = logger;
        }
    }
}