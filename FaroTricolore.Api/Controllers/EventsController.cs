﻿using System;
using System.Threading.Tasks;
using FaroTricolore.Api.Shared.Abstracts;
using FaroTricolore.Api.Shared.Abstracts.Services;
using FaroTricolore.Api.Shared.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using FaroTricolore.Api.Shared.Models;
using Microsoft.AspNetCore.Authorization;

namespace FaroTricolore.Api.Controllers
{
    /// <summary>
    /// Events controller.
    /// </summary>
    [Route("v1/events")]
    public class EventsController : BaseController
    {
        private readonly IEventService _eventService;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:FaroTricolore.Api.EventsController"/> class.
        /// </summary>
        public EventsController(ILogger<EventsController> logger, 
            IEventService eventService) : base(logger)
        {
            this._eventService = eventService;
        }
        
        
        /// <summary>
        /// Add event.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = Roles.Admin + "," + Roles.EventManager)]
        public async Task<IActionResult> InsertEvent([FromBody] EventToAddJson eventToAddJson)
        {
            try
            {
                await _eventService.InsertEventAsync(eventToAddJson);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Edit an event
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = Roles.Admin + "," + Roles.EventManager)]
        public async Task<IActionResult> EditEvent([FromBody] EventToAddJson eventToAddJson)
        {
            try
            {
                await _eventService.UpdateEventAsync(eventToAddJson);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Delete event by id.
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        [HttpDelete("{eventId}")]
        [Authorize(Roles = Roles.Admin + "," + Roles.EventManager)]
        public async Task<IActionResult> DeleteEventById([FromRoute] string eventId)
        {
            try
            {
                await _eventService.DeleteEventAsync(eventId);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}
