using System;
using FaroTricolore.Api.MongoDb.Documents;
using FaroTricolore.Api.Shared.Models;

namespace FaroTricolore.Api.MongoDb.Factories
{
    public static class VideoDtoFactory
    {
        public static VideoDto ToDto(this VideoJson json)
        {
            return new VideoDto
            {
                Id = string.IsNullOrWhiteSpace(json.Id) ? Guid.NewGuid().ToString("N") : json.Id,
                YouTubeCode = json.YouTubeCode
            };
        }
        
        public static VideoJson ToJson(this VideoDto dto)
        {
            return new VideoJson
            {
                Id = dto.Id,
                YouTubeCode = dto.YouTubeCode
            };
        }
    }
}