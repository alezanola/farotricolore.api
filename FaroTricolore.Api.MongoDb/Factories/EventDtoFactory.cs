using System;
using System.Linq;
using FaroTricolore.Api.MongoDb.Documents;
using FaroTricolore.Api.Shared.Models;
using FaroTricolore.Api.Shared.ValueObjects;

namespace FaroTricolore.Api.MongoDb.Factories
{
    public static class EventDtoFactory
    {
        public static EventDto CreateEventDto(
            DtoId id,
            string name,
            string longDescription,
            string shortDescription,
            DateTime startDate,
            DateTime endDate,
            string location,
            bool hideEvent
            
            ) => 
            new EventDto()
            {
                Id = id.GetValue(),
                Name = name,
                LongDescription = longDescription,
                ShortDescription = shortDescription,
                StartDate = startDate,
                EndDate = endDate,
                Location = location,
                HideEvent = hideEvent
            };
        

        public static EventLiteResponseJson ToLiteJson(this EventDto dto, MediaDto media = null, string storagePath = "")
        {
            return new EventLiteResponseJson()
            {
                Id = dto.Id,
                Name = dto.Name,
                ShortDescription = dto.ShortDescription,
                StartDate = dto.StartDate,
                EndDate = dto.EndDate,
                Location = dto.Location,
                HideEvent = dto.HideEvent,
                Poster = media?.Poster?.ToJson(storagePath)
            };
        }

        public static EventDetailedJson ToDetailedJson(this EventDto dto, MediaDto media, string storagePath)
        {
            return new EventDetailedJson()
            {
                Id = dto.Id,
                Name = dto.Name,
                ShortDescription = dto.ShortDescription,
                LongDescription = dto.LongDescription,
                StartDate = dto.StartDate,
                EndDate = dto.EndDate,
                Location = dto.Location,
                HideEvent = dto.HideEvent,
                Poster = media?.Poster?.ToJson(storagePath),
                MediaExist = (media?.Images?.Any() ?? false) || (media?.Videos?.Any() ?? false)
            };
        }
        
        public static EventWithFirstImageJson ToEventWithFirstImageJson(this EventDto dto, MediaDto mediaDto, string baseUrl)
        {
            return new EventWithFirstImageJson()
            {
                Id = dto.Id,
                Name = dto.Name,
                Location = dto.Location,
                StartDate = dto.StartDate,
                FirstImage = mediaDto.Images.FirstOrDefault()?.ToJson(baseUrl)
            };
        }
    }
}