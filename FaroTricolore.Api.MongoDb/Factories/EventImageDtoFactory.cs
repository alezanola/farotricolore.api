using FaroTricolore.Api.MongoDb.Documents;
using FaroTricolore.Api.Shared.Models;

namespace FaroTricolore.Api.MongoDb.Factories
{
    public static class EventImageDtoFactory
    {
        public static ImageJson ToJson(this EventImageDto dto, string storagePath)
        {
            if (string.IsNullOrWhiteSpace(dto.Large) || string.IsNullOrWhiteSpace(dto.Medium) || string.IsNullOrWhiteSpace(dto.Small))
                return null;
            return new ImageJson
            {
                Id = dto.Id,
                Large = storagePath + dto.Large,
                Medium = storagePath + dto.Medium,
                Small = storagePath + dto.Small
            };
        }
    }
}