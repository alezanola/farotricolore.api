using System.Collections.Generic;
using System.Linq;
using FaroTricolore.Api.MongoDb.Documents;
using FaroTricolore.Api.Shared.Models;
using FaroTricolore.Api.Shared.ValueObjects;

namespace FaroTricolore.Api.MongoDb.Factories
{
    public static class MediaDtoFactory
    {
        public static MediaDto CreateDto(DtoId id, EventImageDto poster, 
            IEnumerable<EventImageDto> images = null)

            => new MediaDto()
            {
                Id = id.GetValue(),
                PosterName = poster.Name,
                Poster = poster,
                Images = images
            };

        public static EventMediaJson ToJson(this MediaDto dto, EventDto eventDto, string storagePath)
        {
            return new EventMediaJson
            {
                Id = eventDto.Id,
                Name = eventDto.Name,
                Images = dto.Images?.Select(t => t.ToJson(storagePath)),
                Videos = dto.Videos?.Select(t => t.ToJson())
            };
        }
    }
}