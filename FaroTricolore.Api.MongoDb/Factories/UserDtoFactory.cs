using System;
using FaroTricolore.Api.MongoDb.Documents;
using FaroTricolore.Api.Shared.Models;
using FaroTricolore.Api.Shared.ValueObjects;

namespace FaroTricolore.Api.MongoDb.Factories
{
    public class UserDtoFactory
    {
        public static UserDto CreateUserDto(UserId id, string email, Password password, string role, string name, string surname)
            => new UserDto()
            {
                Id = id.GetValue(),
                Email = email,
                Password = password.GetValue(),
                Role = role,
                Name = name,
                Surname = surname
            };

        public static UserDto CreateUserDtoFromJson(UserJson userJson)
            => new UserDto
            {
                Id = string.IsNullOrEmpty(userJson.Id) ? "" : new UserId(new Guid(userJson.Id)).GetValue(),
                Email = userJson.Email,
                Password = new Password(userJson.Password).GetValue(),
                Role = userJson.Role,
                Name = userJson.Name,
                Surname = userJson.Surname
            };
    }
}