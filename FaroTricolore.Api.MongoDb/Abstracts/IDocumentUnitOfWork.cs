using FaroTricolore.Api.MongoDb.Documents;
using FaroTricolore.Api.Shared.Configuration;
using Microsoft.Extensions.Options;

namespace FaroTricolore.Api.MongoDb.Abstracts
{
    public interface IDocumentUnitOfWork
    {
        IRepository<EventDto> EventRepository { get; }
        IRepository<MediaDto> MediaRepository { get; }
        IRepository<UserDto> UserRepository { get; }
        
    }
}