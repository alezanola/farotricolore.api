using MongoDB.Bson.Serialization.Attributes;

namespace FaroTricolore.Api.MongoDb.Abstracts
{
    public interface IEntity
    {
        [BsonId]
        string Id { get; set; }
    }
}