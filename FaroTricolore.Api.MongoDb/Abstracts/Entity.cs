using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Serializers;

namespace FaroTricolore.Api.MongoDb.Abstracts
{
    public class Entity : IEntity
    {
        [BsonId]
        public string Id { get; set; }

        protected Entity()
        {
        }
    }
}