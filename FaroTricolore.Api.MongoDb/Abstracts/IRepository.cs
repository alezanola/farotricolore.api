using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace FaroTricolore.Api.MongoDb.Abstracts
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        void StartTransaction();
        Task CommitTransactionAsync();
        Task AbortTransactionAsync();
        Task<string> InsertOneAsync(TEntity documentToInsert);
        Task ReplaceOneAsync(FilterDefinition<TEntity> filter, TEntity documentToUpdate);
        Task DeleteOneAsync(Expression<Func<TEntity, bool>> filter);
        Task DeleteOneByIdAsync(string id);
        Task<IList<TEntity>> FindAsync(Expression<Func<TEntity, bool>> filter = null, Expression<Func<TEntity, object>> orderBy = null, 
            bool descending = false, int? qty = null, int? page = null);
        Task UpdateOneAsync(FilterDefinition<TEntity> filter, UpdateDefinition<TEntity> updateDefinition);
        Task<ReplaceOneResult> SaveAsync(TEntity documentToSave);
        Task<TEntity> GetByIdAsync(string id);
    }
}