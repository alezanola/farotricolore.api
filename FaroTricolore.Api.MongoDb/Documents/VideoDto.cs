namespace FaroTricolore.Api.MongoDb.Documents
{
    public class VideoDto
    {
        public string Id { get; set; }
        public string YouTubeCode { get; set; }
    }
}