using System;
using System.Linq;
using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.MongoDb.Factories;
using FaroTricolore.Api.Shared.Models;
using FaroTricolore.Api.Shared.ValueObjects;

namespace FaroTricolore.Api.MongoDb.Documents
{
    public class EventDto : Entity
    {
        public string Name { get; protected internal set; }
        public string LongDescription { get; protected internal set; }
        public string ShortDescription { get; protected internal set; }
        public DateTime? StartDate { get; protected internal set; }
        public DateTime? EndDate { get; protected internal set; }
        public string Location { get; protected internal set; }
        public bool HideEvent { get; protected internal set; }

        public static EventDto CreateNoSqlEventFromJson(EventToAddJson eventToAddJson)
        {
            return CreateNoSqlEventFromJson(eventToAddJson, Guid.Empty);
        }

        public static EventDto CreateNoSqlEventFromJson(EventToAddJson eventToAddJson, Guid id)
        {
            var eventId = new DtoId(id != Guid.Empty ? id : new Guid(eventToAddJson.Id));

            return new EventDto {
                Id = eventId.GetValue(),
                Name = eventToAddJson.Name,
                LongDescription = eventToAddJson.LongDescription,
                ShortDescription = eventToAddJson.ShortDescription,
                StartDate = eventToAddJson.StartDate,
                EndDate = eventToAddJson.EndDate,
                Location = eventToAddJson.Location,
                HideEvent = eventToAddJson.HideEvent ?? false
            };
        }
    }
}