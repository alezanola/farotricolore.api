namespace FaroTricolore.Api.MongoDb.Documents
{
    public class EventImageDto
    {
        public string Id { get; set; }
        /// <summary> Old path </summary>
        public string Name { get; set; }
        public string Large { get; set; }
        public string Medium { get; set; }
        public string Small { get; set; }
    }
}