using System;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.Shared.Models;

namespace FaroTricolore.Api.MongoDb.Documents
{
    public class UserDto : Entity
    {
        public string Email { get; protected internal set; }
        public string Password { get; protected internal set; }
        public string Role { get; protected internal set; }
        public string Name { get; protected internal set; }
        public string Surname { get; protected internal set; }

        public void SetHashedPassword(string password)
        {
            var data = Encoding.UTF8.GetBytes(password);
            using (HashAlgorithm sha = new SHA256Managed())
            {
                sha.TransformFinalBlock(data, 0, data.Length);
                Password = Convert.ToBase64String(sha.Hash);
            }
        }
        
        public UserJson ToJson()
        {
            return new UserJson()
            {
                Id = Id,
                Email = Email,
                Role = Role,
                Name = Name,
                Surname = Surname
            };
        }
    }
}