using System.Collections.Generic;
using FaroTricolore.Api.MongoDb.Abstracts;

namespace FaroTricolore.Api.MongoDb.Documents
{
    public class MediaDto : Entity
    {
        public string EventPath { get; set; }
        /// <summary> Old path </summary>
        public string PosterName { get; set; }
        /// <summary> New path </summary>
        public EventImageDto Poster { get; set; }
        public IEnumerable<EventImageDto> Images { get; set; }
        public IEnumerable<VideoDto> Videos { get; set; }
    }
}