using System;
using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.Shared.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace FaroTricolore.Api.MongoDb.Repository
{
    public class MongoConnectionHandler<TEntity> where TEntity : IEntity
    {
        public IMongoCollection<TEntity> MongoCollection { get; }
        public IClientSessionHandle MongoSession { get; }

        public MongoConnectionHandler(IOptions<AppSettings> authOptions)
        {
            var faroSettings = authOptions.Value;

            try
            {
                var mongoClientSettings = 
                    MongoClientSettings.FromUrl(new MongoUrl(faroSettings.ConnectionStrings.MongoDb));

                var mongoClient = new MongoClient(mongoClientSettings);
                MongoSession = mongoClient.StartSession();
                
                var mongoDatabase = mongoClient.GetDatabase(faroSettings.ConnectionStrings.DatabaseName);
                
                var typeName = typeof(TEntity).Name.ToLower();
                typeName = typeName.Replace("dto", "");

                this.MongoCollection = mongoDatabase.GetCollection<TEntity>(typeName + "sCollection");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw new Exception(ex.Message);
            }
        }
    }
}