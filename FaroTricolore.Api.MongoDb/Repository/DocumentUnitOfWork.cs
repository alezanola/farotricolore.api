using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.MongoDb.Documents;
using FaroTricolore.Api.Shared.Configuration;
using Microsoft.Extensions.Options;

namespace FaroTricolore.Api.MongoDb.Repository
{
    public class DocumentUnitOfWork : IDocumentUnitOfWork
    {
        private readonly IOptions<AppSettings> _faroOptions;

        public DocumentUnitOfWork(IOptions<AppSettings> faroOptions)
        {
            this._faroOptions = faroOptions;
        }

        private IRepository<EventDto> _noSqlEventRepository;
        public IRepository<EventDto> EventRepository =>
            this._noSqlEventRepository ?? (this._noSqlEventRepository =
                new Repository<EventDto>(_faroOptions));

        
        private IRepository<MediaDto> _noSqlEventImageRepository;
        public IRepository<MediaDto> MediaRepository =>
            _noSqlEventImageRepository ?? (_noSqlEventImageRepository =
                new Repository<MediaDto>(_faroOptions));


        private IRepository<UserDto> _noSqlUserRepository;

        public IRepository<UserDto> UserRepository =>
            _noSqlUserRepository ?? (_noSqlUserRepository =
                new Repository<UserDto>(_faroOptions));
    }
}