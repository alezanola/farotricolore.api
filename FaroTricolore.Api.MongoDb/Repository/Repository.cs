using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using FaroTricolore.Api.MongoDb.Abstracts;
using FaroTricolore.Api.Shared.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace FaroTricolore.Api.MongoDb.Repository
{
    public sealed class Repository<TEntity> : IRepository<TEntity> where TEntity : IEntity 
    {
        private readonly IMongoCollection<TEntity> _collection;
        private readonly IClientSessionHandle _session;

        public Repository(IOptions<AppSettings> faroOptions)
        {
            var mongoConnectionHandler = new MongoConnectionHandler<TEntity>(faroOptions);
            _collection = mongoConnectionHandler.MongoCollection;
            _session = mongoConnectionHandler.MongoSession;
        }

        public void StartTransaction()
        {
            var options = new TransactionOptions(ReadConcern.Majority);
            _collection.WithReadConcern(ReadConcern.Linearizable);
            _session.StartTransaction(options);
        }

        public async Task CommitTransactionAsync()
        {
            if(_session.IsInTransaction) 
                await _session.CommitTransactionAsync();
        }

        public async Task AbortTransactionAsync()
        {
            if (_session.IsInTransaction)
                await _session.AbortTransactionAsync();
        }
        
        

        public async Task<string> InsertOneAsync(TEntity documentToInsert)
        {
            await this._collection.InsertOneAsync(documentToInsert);
            return documentToInsert.Id;
        }

        public async Task ReplaceOneAsync(FilterDefinition<TEntity> filter, TEntity documentToUpdate)
        {
            await this._collection.ReplaceOneAsync(filter, documentToUpdate);
        }

        public async Task DeleteOneAsync(Expression<Func<TEntity, bool>> filter)
        {
            await this._collection.DeleteOneAsync(filter);
        }

        public async Task DeleteOneByIdAsync(string id)
        {
            await this.DeleteOneAsync(entity => entity.Id == id);
        }

        public async Task<IList<TEntity>> FindAsync(Expression<Func<TEntity, bool>> filter = null, Expression<Func<TEntity, object>> orderBy = null, 
            bool descending = false, int? qty = null, int? page = null)
        {
            var options = orderBy == null ? new FindOptions<TEntity>() :
                new FindOptions<TEntity, TEntity>
            {
                Sort = descending ? Builders<TEntity>.Sort.Descending(orderBy) :
                    Builders<TEntity>.Sort.Ascending(orderBy),
                Limit = qty,
                Skip = (page ?? 0) * qty 
            };

            var filterDefinition = filter ?? FilterDefinition<TEntity>.Empty;
            
            var results = await _collection.FindAsync(_session, filterDefinition, options);
            return await results.ToListAsync();
        }

        public async Task UpdateOneAsync(FilterDefinition<TEntity> filter,
            UpdateDefinition<TEntity> updateDefinition)
        {
            await this._collection.UpdateOneAsync(filter, updateDefinition);
        }

        public async Task<ReplaceOneResult> SaveAsync(TEntity documentToSave)
        {
            var replaceOneResult = await this._collection.ReplaceOneAsync(
                _session,
                doc => doc.Id == documentToSave.Id,
                documentToSave,
                new ReplaceOptions { IsUpsert = true });

            return replaceOneResult;
        }

        public async Task<TEntity> GetByIdAsync(string id)
        {
            var results = await this.FindAsync(doc => doc.Id == id);

            return results.FirstOrDefault();
        }
    }
}